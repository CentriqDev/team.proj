﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Team.Proj.UI.Startup))]
namespace Team.Proj.UI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
